import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;


public class Score extends JFrame implements ActionListener{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1080893571786515121L;
	private JLabel lblScore[];
	private JTextField txtScore[], txtWeight[];
	private JButton btnCalculate;
	private JPanel pnlButton, pnlScore;
	
	private final int SIZE = 4;
	
	public Score () {
		pnlButton = new JPanel();
		pnlScore = new JPanel();
		
		lblScore = new JLabel[2];
		
		lblScore[0] = new JLabel();
		lblScore[1] = new JLabel();
		
		lblScore[0].setText("TestScore");
		lblScore[1].setText("Weight");
		pnlScore.add(lblScore[0]);
		pnlScore.add(lblScore[1]);
		
		
		txtScore = new JTextField[SIZE];
		txtWeight = new JTextField[SIZE];
		
		for(int i=0; i<SIZE; i++) {
			txtScore[i] = new JTextField();
			txtScore[i].setBackground(Color.GRAY);
			txtScore[i].setSize(12,50);
			pnlScore.add(txtScore[i]);
			
			txtWeight[i] = new JTextField();
			txtWeight[i].setBackground(Color.ORANGE);
			txtWeight[i].setSize(12,50);
			pnlScore.add(txtWeight[i]);
		}
		
		btnCalculate = new JButton("Calculate");
		btnCalculate.addActionListener(this);
		
		pnlScore.setBackground(Color.CYAN);
		pnlButton.setBackground(Color.GREEN);
		
		GridLayout gLayout = new GridLayout(5,2);
		pnlScore.setLayout(gLayout);
		//add labels and text fields to the center of the frame
		
		pnlButton.add(btnCalculate); //btnCalculate is added
		
		add(pnlButton, BorderLayout.SOUTH);
		add(pnlScore, BorderLayout.CENTER);
		
		setTitle("Score Calculator");
		setVisible(true);
		setSize(860,480);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);//Windows open in the center of the screen
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Score();
	}
	
	public void actionPerformed(ActionEvent e) {
		String Sscore1 = txtScore[0].getText();
		String Sscore2 = txtScore[1].getText();
		String Sscore3 = txtScore[2].getText();
		String Sscore4 = txtScore[3].getText();
		String Sweight1 = txtWeight[0].getText();
		String Sweight2 = txtWeight[1].getText();
		String Sweight3 = txtWeight[2].getText();
		String Sweight4 = txtWeight[3].getText();
		double score1 = Double.parseDouble(Sscore1);
		double score2 = Double.parseDouble(Sscore2);
		double score3 = Double.parseDouble(Sscore3);
		double score4 = Double.parseDouble(Sscore4);
		double weight1 = Double.parseDouble(Sweight1);
		double weight2 = Double.parseDouble(Sweight2);
		double weight3 = Double.parseDouble(Sweight3);
		double weight4 = Double.parseDouble(Sweight4);
		double answer = ((score1*weight1)+(score2*weight2)+(score3*weight3)+(score4*weight4));
		JOptionPane.showMessageDialog(null, "The Average of Test Grades is: " + answer);
	}

}